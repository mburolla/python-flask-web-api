# Python Flask Web API
A simple web API using Python Flask.

# Getting Started Local Development
- Clone this repo
- `python3 -m venv venv`
- `. venv/bin/activate`
- `python app.py`

# EC2 Deployment Steps
- SSH to server: `ssh -i "sia-ec2-key.pem" ec2-user@ec2-3-216-185-158.compute-1.amazonaws.com`
- Change user to root: `sudo su`
- Create directory <your name>
- `cd` into your directory
- Initialize this directory: `git init`
- Clone this repo `git clone git@gitlab.com:mburolla/python-flask-web-api.git`
- Install dependencies:
  - `cd python-flask-web-api`
  - `pip3 install -r requirements.txt`
- Change port number line 40: `nano app.py` to your assigned port number (5000 + student number) e.g. 5001
- Save file `app.py` and exit nano
- Execute `run.sh`
- Is the API running on your port? `netstat -plutn`
- Optionally, check forever processes: `forever list`

# Run in Background
- Install Node
  - curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash -
  - sudo yum install nodejs
- Install Forever
  - npm install forever -g
- Execute `run.sh`
 
# Testing
- `netstat -plutn`
- GET `curl -v http://127.0.0.1:5150/car/1`
- `forever list`
- `forever stopall`

# Misc
- Built with VS Code
- Python 3.8.5
- https://flask.palletsprojects.com/en/2.0.x/installation/
- https://www.youtube.com/watch?v=kvux1SiRIJQ

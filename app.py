# 
# File: app.py
# Auth: Martin Burolla
# Date: 9/13/2021
# Desc: Simple Flask Web API.
# 

from waitress import serve
from flask import Flask, jsonify, request

app = Flask(__name__)

@app.after_request
def apply_caching(response):
    response.headers['env'] = 'dev'
    return response

#########################################################
# GET /car/<id>
#########################################################

@app.route('/car/<carId>', methods=['GET'])
def getCar(carId):
  car = { 
    'id' : int(carId),
    'name': 'mustang'
  }
  return jsonify(car)

#########################################################
# POST /car
#########################################################

@app.route('/car', methods=['POST'])  
def postCar():
  data = request.json
  return jsonify({'echo' : data})

if __name__ == "__main__": 
  serve(app, host = "172.31.84.39", port = 5000)
  #app.run(debug = True)
